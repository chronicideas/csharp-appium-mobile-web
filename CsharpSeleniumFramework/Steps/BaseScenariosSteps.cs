﻿using CsharpSeleniumFramework.Pages;
using TechTalk.SpecFlow;

namespace CsharpSeleniumFramework.Steps
{
    [Binding]
    public sealed class BaseScenariosSteps : BaseSteps
    {
        [Given(@"I navigate to the base URL")]
        public void GivenINavigateToTheBaseUrl()
        {
            InstanceOf<BasePage>().NavigateBaseUrl();
        }
    }
}
