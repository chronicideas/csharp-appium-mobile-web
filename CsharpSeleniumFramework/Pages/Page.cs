﻿using CsharpSeleniumFramework.Utils.Selenium;
using static SeleniumExtras.PageObjects.PageFactory;

namespace CsharpSeleniumFramework.Pages
{
    public abstract class Page
    {
        protected T InstanceOf<T>() where T : BasePage, new()
        {
            var pageClass = new T { Driver = Driver.Browser() };
            InitElements(Driver.Browser(), pageClass);

            return pageClass;
        }
    }
}
