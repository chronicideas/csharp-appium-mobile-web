﻿

using NUnit.Framework;
using OpenQA.Selenium;

namespace CsharpSeleniumFramework.Pages
{
    public class RedditPage : SearchResultsPage
    {
        public IWebElement RedditContentArea => Driver.FindElement(By.Id("SHORTCUT_FOCUSABLE_DIV"));

        public void AssertRedditPageDisplayed()
        {
            Assert.True(RedditContentArea.Displayed);
        }
    }
}