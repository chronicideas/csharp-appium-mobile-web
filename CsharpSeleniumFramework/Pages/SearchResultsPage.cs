﻿using System.Collections.Generic;
using CsharpSeleniumFramework.Utils.Extensions;
using NUnit.Framework;
using OpenQA.Selenium;

namespace CsharpSeleniumFramework.Pages
{
    public class SearchResultsPage : BasePage
    {
        public IWebElement SearchResultsContainer => Driver.FindElement(By.Id("links"));
        public IList<IWebElement> SearchResults => Driver.FindElements(By.ClassName("result__a"));

        public RedditPage SelectFirstListedSearchResult()
        {
            SearchResults[0].WeClick();
            return InstanceOf<RedditPage>();
        }

        public void AssertSearchResultsDisplayed()
        {
            Assert.True(SearchResultsContainer.Displayed);
        }
    }
}