﻿using OpenQA.Selenium;
using TechTalk.SpecFlow;

namespace CsharpSeleniumFramework.Utils.Selenium
{
    [Binding]
    internal class Driver
    {
        internal static IWebDriver Browser()
        {
            return DriverController.Instance.WebDriver;
        }
    }
}
