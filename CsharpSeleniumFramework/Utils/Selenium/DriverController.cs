﻿using System;
using System.Diagnostics;
using CsharpSeleniumFramework.Utils.Drivers;
using OpenQA.Selenium;

namespace CsharpSeleniumFramework.Utils.Selenium
{
    internal class DriverController
    {
        internal static DriverController Instance = new DriverController();
        internal IWebDriver WebDriver { get; set; }

        internal void StartChrome(String chromeArgument)
        {
            if (WebDriver != null) return;
            WebDriver = ChromeWebDriver.LoadChromeDriver(chromeArgument);
        }

        internal void StartFirefox(String firefoxArgument)
        {
            if (WebDriver != null) return;
            WebDriver = FirefoxWebDriver.LoadFirefoxDriver(firefoxArgument);
        }

        internal void StopWebDriver()
        {
            if (WebDriver == null) return;

            try
            {
                WebDriver.Quit();
                WebDriver.Dispose();
            }
            catch (Exception e)
            {
                Debug.WriteLine(e, ":: WebDriver stop error");
                throw;
            }

            WebDriver = null;
            Console.WriteLine(":: WebDriver stopped");
        }
    }
}
