﻿using CsharpSeleniumFramework.Utils.Selenium;
using TechTalk.SpecFlow;
using System.Linq;

namespace CsharpSeleniumFramework.Utils.Hooks
{
    [Binding]
    internal static class FeatureHooks
    {
        [BeforeFeature]
        internal static void StartWebDriver()
        {
            if (FeatureContext.Current.FeatureInfo.Tags.Equals("Chrome"))
            {
                DriverController.Instance.StartChrome("--disable-extensions");
            }
            else if (FeatureContext.Current.FeatureInfo.Tags.Equals("Firefox"))
            {
                DriverController.Instance.StartFirefox("--disable-extensions");
            }
            else if (FeatureContext.Current.FeatureInfo.Tags.Equals("HeadlessChrome"))
            {
                DriverController.Instance.StartChrome("--headless");
            }
            else if (FeatureContext.Current.FeatureInfo.Tags.Equals("HeadlessFirefox"))
            {
                DriverController.Instance.StartFirefox("--headless");
            }
            else if (FeatureContext.Current.FeatureInfo.Tags.Equals("Debug"))
            {
                DriverController.Instance.StartChrome("--disable-extensions");
            }
            else
            {
                DriverController.Instance.StartChrome("--disable-extensions");
            }
        }

        [AfterFeature]
        internal static void StopWebDriver()
        {
            if (!FeatureContext.Current.FeatureInfo.Tags.Contains("Debug"))
                DriverController.Instance.StopWebDriver();
        }
    }
}
