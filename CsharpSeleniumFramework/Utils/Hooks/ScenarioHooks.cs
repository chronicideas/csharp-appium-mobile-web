﻿using System.Linq;
using CsharpSeleniumFramework.Utils.Selenium;
using TechTalk.SpecFlow;

namespace CsharpSeleniumFramework.Utils.Hooks
{
    [Binding]
    internal static class ScenariosHooks
    {
        [BeforeScenario]
        internal static void StartWebDriver()
        {
            if (ScenarioContext.Current.ScenarioInfo.Tags.Equals("Chrome"))
            {
                DriverController.Instance.StartChrome("--disable-extensions");
            }
            else if (ScenarioContext.Current.ScenarioInfo.Tags.Equals("Firefox"))
            {
                DriverController.Instance.StartFirefox("--disable-extensions");
            }
            else if (ScenarioContext.Current.ScenarioInfo.Tags.Equals("HeadlessChrome"))
            {
                DriverController.Instance.StartChrome("--headless");
            }
            else if (ScenarioContext.Current.ScenarioInfo.Tags.Equals("HeadlessFirefox"))
            {
                DriverController.Instance.StartFirefox("--headless");
            }
            else if (ScenarioContext.Current.ScenarioInfo.Tags.Equals("Debug"))
            {
                DriverController.Instance.StartChrome("--disable-extensions");
            }
            else
            {
                DriverController.Instance.StartChrome("--disable-extensions");
            }
        }

        [AfterScenario]
        internal static void StopWebDriver()
        {
            if (!ScenarioContext.Current.ScenarioInfo.Tags.Contains("Debug"))
                DriverController.Instance.StopWebDriver();
        }
    }
}
