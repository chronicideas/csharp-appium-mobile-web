﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace CsharpSeleniumFramework.Utils.Drivers
{
    internal static class ChromeWebDriver
    {
        internal static IWebDriver LoadChromeDriver(String chromeArgument)
        {
            var driverService = ChromeDriverService.CreateDefaultService();
            driverService.HideCommandPromptWindow = true;

            var options = new ChromeOptions();
            options.AddArgument(chromeArgument);

            var driver = new ChromeDriver(driverService, options);
            return driver;
        }
    }
}
